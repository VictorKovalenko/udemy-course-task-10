import 'package:flutter/material.dart';
import 'package:multiplication/provider.dart';
import 'package:provider/provider.dart';

import 'home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => MyProvider(),
        builder: (conteext, child) => MaterialApp(
              title: 'Flutter Demo',
              theme: ThemeData.dark(),
              home: MyHomePage(),
            ));
  }
}
