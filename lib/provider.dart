import 'package:flutter/cupertino.dart';

class MyProvider with ChangeNotifier{
  String _appBar='';

  String get appBar => _appBar;

  void changeAppBar (String text){
    _appBar=text;
    notifyListeners();
  }
}
