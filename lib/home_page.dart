import 'package:flutter/material.dart';
import 'package:multiplication/provider.dart';
import 'package:provider/provider.dart';

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

  var provider = Provider.of<MyProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(provider.appBar),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.black26,
        child: Column(
          children: [
            SizedBox(
              height: 400,
            ),
            Row(
              children: [
                SizedBox(
                  width: 50,
                ),
                ElevatedButton(
                  onPressed: () {
                  provider.changeAppBar('One');
                  },
                  child: Text('One'),
                ),
                Expanded(
                  child: Container(),
                ),
                ElevatedButton(
                  onPressed: () {
                    provider.changeAppBar('Two');
                  },
                  child: Text('Two'),
                ),
                Expanded(
                  child: Container(),
                ),
                ElevatedButton(
                  onPressed: () {
                    provider.changeAppBar('Three');
                  },
                  child: Text('Three'),
                ),
                Expanded(
                  child: Container(),
                ),
                ElevatedButton(
                  onPressed: () {
                    provider.changeAppBar('Four');
                  },
                  child: Text('Four'),
                ),
                SizedBox(
                  width: 50,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

//ChangeNotifierProvider<MyProvider>(
//       create: (context) => MyProvider(),
//       builder: (context, child) =>

//var provider = Provider.of<MyProvider>(context);
//import 'package:flutter/cupertino.dart';
//
// class MyProvider with ChangeNotifier{
//   String _title = '';
//   String get title => _title;
//
//   void changeTitle(String text){
//     _title = text;
//     notifyListeners();
//   }
// }
